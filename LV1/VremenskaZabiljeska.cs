﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class VremenskaZabiljeska: Zabiljeska
    {
        private DateTime vrijeme;
        public VremenskaZabiljeska(DateTime v, string t, string a, int r) : base(t, a, r)
        {
            vrijeme = v;
        }
        public VremenskaZabiljeska(string t, string a, int r) : base(t, a, r)
        {
            vrijeme = DateTime.Now;
        }

        public DateTime Vrijeme
        {
            get { return this.vrijeme; }
            set { this.vrijeme = value; }
        }
        public override string ToString()
        {
            return ("Autor:" + base.Autor + ", Tekst:" + base.Tekst + ", Vrijeme: " +vrijeme.ToString());
        }
    }
}
