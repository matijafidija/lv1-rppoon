﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Zabiljeska{
        private String tekst;
        private String autor;
        private int razinaVaznosti;

        public string GetTekst()
        {
            return tekst;
        }
        public string GetAutor()
        {
            return autor;
        }
        public int GetRazinaVaznosti()
        {
            return razinaVaznosti;
        }

        public void SetTekst(string t)
        {
            tekst = t;
        }
        public void SetAutor(string a)
        {
            autor = a;
        }
        public void SetRazinaVaznosti(int r)
        {
            razinaVaznosti = r;
        }

 
        public string Tekst
        {
            get { return this.tekst; }
            set { this.tekst = value; }
        }
        public string Autor
        {
            get { return this.autor; }
            set { this.autor = value; }
        }

        public int RazinaVaznosti 
        {
            get { return this.razinaVaznosti; }
            set { this.razinaVaznosti = value; }
        }
        public Zabiljeska(string t, string a, int r)
        {
            tekst = t;
            autor = a;
            razinaVaznosti = r;
        }
        public Zabiljeska(string t, string a)
        {
            tekst = t;
            autor = a;
            razinaVaznosti = 0;
        }
        public Zabiljeska(string t, int r)
        {
            tekst = t;
            autor = "-";
            razinaVaznosti = r;
        }

        public override string ToString()
        {
            return ("Autor:" +autor+ ", Sadrzaj" +tekst+);
        }
    }
}
