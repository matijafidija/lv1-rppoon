﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1
{
    class Program
    {     
        static void Main(string[] args)
        {
            Zabiljeska prva = new Zabiljeska("RPPOON", "programer", 3);
            Zabiljeska druga = new Zabiljeska("Truba", "Skladatelj");
            Zabiljeska treca = new Zabiljeska("Obavijest", "Spijun");
            Console.WriteLine("Prva zabiljeska- " + prva.GetTekst() + ", Autor: " + prva.GetAutor());
            Console.WriteLine("Druga zabiljeska- " + druga.GetTekst() + ", Autor: " + druga.GetAutor());
            Console.WriteLine("Treca zabiljeska- " + treca.GetTekst() + ", Autor: " + treca.GetAutor());
            VremenskaZabiljeska cetvrta = new VremenskaZabiljeska("Bum", "Tum", 3);
            Console.WriteLine(cetvrta.ToString());
            Console.ReadLine();
           
        }
    }
}
